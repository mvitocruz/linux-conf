# set paths
PATH=~/bin:~/.cabal/bin:/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin
export PATH

# set useful aliases
alias la="ls -a"
alias lf="ls -FA"
alias ll="ls -lA"
#alias ls="ls --color=auto"
alias du="du -h"
alias df="df -h"

# set useful options
if [[ -o interactive ]]; then
	# set history options
	setopt HIST_IGNORE_DUPS
	setopt HIST_FIND_NO_DUPS
	setopt INC_APPEND_HISTORY
	#setopt SHARE_HISTORY
	setopt EXTENDED_HISTORY
	setopt HIST_VERIFY
	HISTSIZE=1000
	SAVEHIST=1000
	# use .zhistory so that other shells won't be confused by syntax
	HISTFILE=~/.zhistory

	setopt NO_NOTIFY
	bindkey -e
	EDITOR=vi
	export EDITOR

	setopt PROMPT_SUBST

	# %F{...}, %f -- set foreground color, reset color to default
	# %B, %b      -- set bold, reset bold
	# %K{...}, %k -- set background color, reset color to default 
	# colors: red, green, yellow, blue, magenta, cyan, white, black
	autoload -U colors
	colors

	autoload -Uz vcs_info
	zstyle ':vcs_info:*' check-for-changes false
	zstyle ':vcs_info:*' enable git hg svn
	precmd () {
		zstyle ':vcs_info:*' formats ' (%F{green}%b%f)'
		vcs_info
	}

	PS1='%F{yellow}%m%f:%1~${vcs_info_msg_0_}%# '
fi

# set useful variables
RSYNC_RSH=ssh
export RSYNC_RSH

autoload -U select-word-style
select-word-style bash

# setup host list for autocompletion
hosts=(${${${${(f)"$(<$HOME/.ssh/known_hosts)"}:#[0-9]*}%%\ *}%%,*})
zstyle ':completion:*:hosts' hosts $hosts

# The following lines were added by compinstall

zstyle ':completion:*' completer _complete
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt '%SAt %p: Hit TAB for more, or the character to insert%s'
zstyle ':completion:*' matcher-list '' '' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*'
zstyle ':completion:*' menu select=0
zstyle ':completion:*' select-prompt '%SScrolling active: current selection at %p%s'
zstyle :compinstall filename '/Users/vitocruz/.zshrc'

autoload -U compinit
compinit
# End of lines added by compinstall
