; use visual bell
;(setq visible-bell t)

; no splash screen
(setq inhibit-splash-screen t)

; set default size and font
(setq default-frame-alist '((width . 120)
                            (height . 60)
                            (top . 0)
                            (left . 0)
                           ))

; no toolbar
(tool-bar-mode -1)

; turn on syntax coloring
(global-font-lock-mode t)
(setq font-lock-maximum-decoration t)

; enable visual feedback on selections
(setq transient-mark-mode t)

; activate column-number mode
(column-number-mode t)

; disable tabs
; (if tabs are required use CTRL-q TAB)
(setq-default indent-tabs-mode nil)

; never create backup files
(setq make-backup-files nil)

; where the site elisp files are
(add-to-list 'load-path "~/elisp")

; packages
(require 'package)
(add-to-list 'package-archives
             '("marmalade" . "http://marmalade-repo.org /packages/"))
(add-to-list 'package-archives
             '("melpa" . "http://melpa.milkbox.net/packages/"))
(package-initialize)

; for color themes
;(load-theme 'deeper-blue t)
(load-theme 'tango-dark t)
;(load-theme 'tsdh-dark t)

; whitespace highlighting
(require 'show-wspace)
(add-hook 'font-lock-mode-hook 'show-ws-highlight-trailing-whitespace)
;(add-hook 'font-lock-mode-hook 'show-ws-highlight-tabs)

; remote file editing
(require 'tramp)
(setq tramp-default-method "scpc")

; save file position
(require 'saveplace)
; to enable by default, uncomment line below,
; otherwise use M-x toggle-save-place
;(setq-default save-place t)

; for AUCTeX
;(require 'tex-site)

; for Haskell
(load "~/elisp/haskell-mode/haskell-site-file")
;(setq auto-mode-alist
;      (append auto-mode-alist
;              '(("\\.[hg]s$"  . haskell-mode)
;                ("\\.hi$"     . haskell-mode)
;                ("\\.l[hg]s$" . literate-haskell-mode))))
;(autoload 'haskell-mode "haskell-mode"
;    "Major mode for editing Haskell scripts." t)
;(autoload 'literate-haskell-mode "haskell-mode"
;    "Major mode for editing literate Haskell scripts." t)
;(add-hook 'haskell-mode-hook 'turn-on-haskell-font-lock)
;(add-hook 'haskell-mode-hook 'turn-on-haskell-decl-scan)
(add-hook 'haskell-mode-hook 'turn-on-haskell-doc-mode)
(add-hook 'haskell-mode-hook 'turn-on-haskell-indentation)
;(add-hook 'haskell-mode-hook 'turn-on-haskell-indent)
;(add-hook 'haskell-mode-hook 'turn-on-haskell-simple-indent)
;(add-hook 'haskell-mode-hook 'turn-on-haskell-ghci)
(add-hook 'haskell-mode-hook 'imenu-add-menubar-index)

; for Erlang
;(add-to-list 'load-path "/opt/local/lib/erlang/lib/tools-2.6.6.4/emacs")
;(setq erlang-root-dir "/opt/local/lib/erlang")
;(add-to-list 'exec-path "/opt/local/lib/erlang/bin")
;(require 'erlang-start)

; for ProofGeneral
(load "~/elisp/ProofGeneral/generic/proof-site.el")

; for ESS
(setq-default inferior-R-program-name "R")
(load "~/elisp/ess/lisp/ess-site.el")
(ess-toggle-underscore nil)

; for Ocaml/Tuareg
(add-to-list 'load-path "~/elisp/tuareg")
(autoload 'tuareg-mode "tuareg" "Major mode for editing Caml code" t)
(autoload 'camldebug "camldebug" "Run the Caml debugger" t)
;(autoload 'tuareg-imenu-set-imenu "tuareg-imenu" "Configuration of imenu for tuareg" t)
(add-hook 'tuareg-mode-hook 'tuareg-imenu-set-imenu)
(setq auto-mode-alist
  (append '(("\\.ml[ily]?$" . tuareg-mode)
            ("\\.topml$" . tuareg-mode))
            auto-mode-alist))

; for scheme
(require 'quack)

; use Octave mode instead of Objective-C for .m files
(setq auto-mode-alist
  (append '(("\\.m$" . octave-mode))
            auto-mode-alist))

(custom-set-variables
  ;; custom-set-variables was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 '(TeX-show-compilation t)
 '(column-number-mode t)
 '(quack-programs (quote ("~/bin/mech-scheme" "bigloo" "csi" "csi -hygienic" "gosh" "gsi" "gsi ~~/syntax-case.scm -" "guile" "kawa" "mit-scheme" "mred -z" "mzscheme" "mzscheme -il r6rs" "mzscheme -il typed-scheme" "mzscheme -M errortrace" "mzscheme3m" "mzschemecgc" "rs" "scheme" "scheme48" "scsh" "sisc" "stklos" "sxi")))
 '(show-paren-mode t)
 '(tool-bar-mode nil)
 '(transient-mark-mode t))
(custom-set-faces
  ;; custom-set-faces was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 )
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)
