#!/bin/sh

if [ -z "$1" ]; then
    VBOX_SHARED=shared
else
    VBOX_SHARED=$1
fi
sudo mount -t vboxsf -o uid=1000,gid=1000 $VBOX_SHARED /mnt/$VBOX_SHARED
