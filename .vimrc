" An example for a vimrc file.
"
" Maintainer:	Bram Moolenaar <Bram@vim.org>
" Last change:	2001 Jul 18
"
" To use it, copy it to
"     for Unix and OS/2:  ~/.vimrc
"	      for Amiga:  s:.vimrc
"  for MS-DOS and Win32:  $VIM\_vimrc
"	    for OpenVMS:  sys$login:.vimrc

" When started as "evim", evim.vim will already have done these settings.
if v:progname =~? "evim"
  finish
endif

" Use Vim settings, rather then Vi settings (much better!).
" This must be first, because it changes other options as a side effect.
set nocompatible

" allow backspacing over everything in insert mode
set backspace=indent,eol,start

set autoindent		" always set autoindenting on
if has("vms")
  set nobackup		" do not keep a backup file, use versions instead
else
  set backup		" keep a backup file
endif
set history=50		" keep 50 lines of command line history
set ruler		" show the cursor position all the time
set showcmd		" display incomplete commands
set incsearch		" do incremental searching
set showmatch		" show matching brackets

" For Win32 GUI: remove 't' flag from 'guioptions': no tearoff menu entries
" let &guioptions = substitute(&guioptions, "t", "", "g")

" Don't use Ex mode, use Q for formatting
map Q gq

" Make p in Visual mode replace the selected text with the "" register.
vnoremap p <Esc>:let current_reg = @"<CR>gvs<C-R>=current_reg<CR><Esc>

" This is an alternative that also works in block mode, but the deleted
" text is lost and it only works for putting the current register.
"vnoremap p "_dp

" Switch syntax highlighting on, when the terminal has colors
" Also switch on highlighting the last used search pattern.
if &t_Co > 2 || has("gui_running")
  syntax on
  set hlsearch
  colorscheme darkblue
endif

" Set the GUI font
set guifont=Monaco:h10

" Set the statusline
set laststatus=2
set statusline=%-t\ %y%(\ [%R%M]%)\ {%b:%B}\ %=L:%l/%L\ C:%c\ %p%%
" %-t          -- left align (-) and print filename
" %y           -- file type
" %(\ [%R%M]%) -- group (disappears if %R and %M are empty)
"                 %R is read-only flag, %M is modified flag
" %b:%B        -- value of character under cursor (in decimal and hex)
" %=           -- begin right alignment
" %l/%L        -- line number and total number of lines
" %c           -- column number
" %p           -- percentage through file

" Only do this part when compiled with support for autocommands.
if has("autocmd")

  " Enable file type detection.
  " Use the default filetype settings, so that mail gets 'tw' set to 72,
  " 'cindent' is on in C files, etc.
  " Also load indent files, to automatically do language-dependent indenting.
  filetype plugin indent on

  " For all text files set 'textwidth' to 78 characters.
  autocmd FileType text setlocal textwidth=78

  " Nice settings for editing Python and other files
  autocmd FileType python call FT_set_tabs()
  autocmd FileType lex call FT_set_tabs()
  autocmd FileType awk call FT_set_tabs()
  autocmd FileType cpp call FT_set_tabs()
  autocmd FileType c call FT_set_tabs() 
  autocmd FileType objc call FT_set_tabs()
  autocmd FileType scheme call FT_set_tabs()
  autocmd FileType haskell call FT_set_tabs()
  autocmd FileType html call FT_set_tabs()
  autocmd FileType r call FT_set_tabs()
  function FT_set_tabs()
    setlocal shiftwidth=4 expandtab smarttab
  endfunction

  " When editing a file, always jump to the last known cursor position.
  " Don't do it when the position is invalid or when inside an event handler
  " (happens when dropping a file on gvim).
  autocmd BufReadPost *
    \ if line("'\"") > 0 && line("'\"") <= line("$") |
    \   exe "normal g`\"" |
    \ endif

  " Set window size for GUI
  autocmd GUIEnter * winsize 80 40

endif " has("autocmd")
